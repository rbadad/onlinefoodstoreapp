package com.online.foodstore.dto;

public class UserOrderReponseDto {

	private String statusCode;
	private String statusMsg;
	public UserOrderReponseDto() {
		super();
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
}
