package com.online.foodstore.dto;

public class OrderitemDto {

	private String itemName;
	private String itemPrice;
	public OrderitemDto() {
		super();
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(String itemPrice) {
		this.itemPrice = itemPrice;
	}
	
}
