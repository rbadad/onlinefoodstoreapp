package com.online.foodstore.dto;

import java.util.List;

public class UserOrderResponseDto {

	private String vendorId;
	private String orderId;
	private String totalAmout;
	private String orderDate;
	private List<OrderitemDto> orderedItems;
	
	public UserOrderResponseDto() {
		super();
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTotalAmout() {
		return totalAmout;
	}

	public void setTotalAmout(String totalAmout) {
		this.totalAmout = totalAmout;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public List<OrderitemDto> getOrderedItems() {
		return orderedItems;
	}

	public void setOrderedItems(List<OrderitemDto> orderedItems) {
		this.orderedItems = orderedItems;
	}
	
}
