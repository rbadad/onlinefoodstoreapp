package com.online.foodstore.service;

import com.online.foodstore.dto.OrderRequestDto;
import com.online.foodstore.dto.ResponseDto;
import com.online.foodstore.entity.User;
import com.online.foodstore.exception.ItemsNotFoundException;
import com.online.foodstore.exception.OrderHistoryNotFound;
import com.online.foodstore.exception.UserNotFoundException;
import com.online.foodstore.exception.VendorNotFoundException;

public interface OrderService {

	public ResponseDto order(OrderRequestDto requestDto) throws UserNotFoundException, VendorNotFoundException, ItemsNotFoundException;
	
	public boolean checkAvailabilityOfOrder(Integer orderId);

	public Object getUserOrderDetails(User user) throws OrderHistoryNotFound;
}
