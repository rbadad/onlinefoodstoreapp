package com.online.foodstore.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.online.foodstore.constants.AppConstants;
import com.online.foodstore.dto.ItemDto;
import com.online.foodstore.dto.OrderRequestDto;
import com.online.foodstore.dto.OrderitemDto;
import com.online.foodstore.dto.ResponseDto;
import com.online.foodstore.dto.UserOrderReponseDto;
import com.online.foodstore.dto.UserOrderResponseDto;
import com.online.foodstore.entity.Item;
import com.online.foodstore.entity.MultipleItems;
import com.online.foodstore.entity.Order;
import com.online.foodstore.entity.User;
import com.online.foodstore.exception.ItemsNotFoundException;
import com.online.foodstore.exception.OrderHistoryNotFound;
import com.online.foodstore.exception.UserNotFoundException;
import com.online.foodstore.exception.VendorNotFoundException;
import com.online.foodstore.repository.MultipleItemRepository;
import com.online.foodstore.repository.OrderRepository;
import com.online.foodstore.repository.UserRepository;

/**
 * 
 * @author janbee, Raja
 *
 */
@Service
public class OrderServiceImpl implements OrderService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);


	@Autowired
	private MultipleItemRepository multipleItemRepository;

	@Autowired
	OrderRepository orderRepository;
	@Autowired
	UserRepository userRepository;
	
	@Override
	public ResponseDto order(OrderRequestDto requestDto) throws UserNotFoundException, VendorNotFoundException, ItemsNotFoundException {
		Order order = new Order();
		ResponseDto response = null ;
		Order orderObj=null;
		Optional<User> user = userRepository.findById(requestDto.getUserId());
		if(!user.isPresent()) {
			throw new UserNotFoundException();
		}
		order.setUser(user.get());
		int vendorId = requestDto.getVendorId();
		if(!checkAvailabilityOfVendor(vendorId)) {
			throw new VendorNotFoundException();
		}
		order.setVendorId(vendorId);
		List<MultipleItems> multipleItemList= new ArrayList<>();
		List<ItemDto> itemDtoList = requestDto.getItems();
		
		if(!checkAvailabilityOfItems(itemDtoList)) {
			throw new ItemsNotFoundException();
		}
		
		order.setTotalAmount(requestDto.getTotalAmount());
		order.setOrderedDate(LocalDate.now());
		orderObj = orderRepository.save(order);
		
		if(orderObj.getOrderId()!=0) {
			for(ItemDto itemDto :itemDtoList) {
				MultipleItems mi = new MultipleItems();
				mi.setItemId(itemDto.getItemId());
				mi.setOrder(orderObj);
				multipleItemList.add(mi);
			}
			multipleItemRepository.saveAll(multipleItemList);
			response = new ResponseDto();
			response.setStatusCode(AppConstants.ORDERED_SUCCESSFULLY_STATUS_CODE);
			response.setStatusMsg(AppConstants.ORDERED_SUCCESSFULLY);
		} else {
			response = new ResponseDto();
			response.setStatusCode(AppConstants.ORDER_CANCELLED_STATUS_CODE);
			response.setStatusMsg(AppConstants.ORDER_CANCELLED);
		}
		return response;
	}


	private boolean checkAvailabilityOfVendor(int vendorId) {
		RestTemplate restTemplate = new RestTemplate();
		String url = AppConstants.CHECK_AVAILABILITY_OF_VENDOR_URL;
		return restTemplate.getForObject(url, Boolean.class, vendorId);
	}
	
	private boolean checkAvailabilityOfItems(List<ItemDto> itemDtoList) {
		RestTemplate restTemplate = new RestTemplate();
		String url = AppConstants.CHECK_AVAILABILITY_OF_ITEMS_URL;
		List<Integer> itemIdList = new ArrayList<>();
		for(ItemDto id : itemDtoList) {
			itemIdList.add(id.getItemId());
		}
		HttpEntity<List<Integer>> entity = new HttpEntity<>(itemIdList, null);
		return restTemplate.postForObject(url, entity, Boolean.class);
	}


	@Override
	public boolean checkAvailabilityOfOrder(Integer orderId) {
		return orderRepository.existsById(orderId);
	}
	

	

	/**
	 * user // passing user object ot get the user order history
	 */
	@Override
	public Object getUserOrderDetails(User user) throws OrderHistoryNotFound {
		LOGGER.info("------------Inside service layer getUserOrderDetails----------- START");
		String itemUrl = "http://localhost:2021/vendor-api/items/";
		UserOrderReponseDto orderResponse = new UserOrderReponseDto();
		UserOrderResponseDto reponseDto = new UserOrderResponseDto();
		List<Order> orderList = new ArrayList<>();
		List<OrderitemDto> orderedItems =  new ArrayList<>();
		try {
			orderList =  orderRepository.findByUser(user);
			if(!orderList.isEmpty()) {
				for (Order order : orderList) {
					OrderitemDto items = new OrderitemDto();
					reponseDto.setOrderId(order.getOrderId().toString());
					reponseDto.setVendorId(order.getVendorId().toString());
					List<MultipleItems> itemss = multipleItemRepository.findByOrder(order);
					for (MultipleItems itm : itemss) {
						RestTemplate restTemplate = new RestTemplate();
						Item item = restTemplate.getForObject(itemUrl+itm.getItemId(), Item.class);
						items.setItemName(item.getItemName());
						items.setItemPrice(String.valueOf(item.getItemPrice()));
						orderedItems.add(items);
						reponseDto.setOrderedItems(orderedItems);
					}
					reponseDto.setTotalAmout(String.valueOf(order.getTotalAmount()));
					reponseDto.setOrderDate(order.getOrderedDate().toString());
				}
				return reponseDto;
			}
			else {
				orderResponse.setStatusCode(AppConstants.NO_ORDER_FOUND_CODE);
				orderResponse.setStatusMsg(AppConstants.NO_ORDER_FOUND_MSG);
				LOGGER.info("------------Inside service layer getUserOrderDetails----------- END");
				return orderResponse;
			}
		} catch (Exception e) {
			orderResponse.setStatusCode(AppConstants.NO_ORDER_FOUND_CODE);
			orderResponse.setStatusMsg(AppConstants.NO_ORDER_FOUND_MSG);
			LOGGER.info("------------Inside service layer getUserOrderDetails----------- END");
			return orderResponse;
		}
	}

}
