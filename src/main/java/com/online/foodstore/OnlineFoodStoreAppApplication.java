package com.online.foodstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineFoodStoreAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineFoodStoreAppApplication.class, args);
	}

}
