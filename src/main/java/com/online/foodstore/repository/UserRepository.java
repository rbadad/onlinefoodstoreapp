package com.online.foodstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.foodstore.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

}
