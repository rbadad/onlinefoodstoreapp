package com.online.foodstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.foodstore.entity.Order;
import com.online.foodstore.entity.User;


/**
 * @author Raja
 *
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Integer>{

	public List<Order> findByUser(User user);

}
