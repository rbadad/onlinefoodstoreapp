package com.online.foodstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.foodstore.entity.MultipleItems;
import com.online.foodstore.entity.Order;

@Repository
public interface MultipleItemRepository extends JpaRepository<MultipleItems, Integer>{

	public List<MultipleItems> findByOrder(Order order);

}
