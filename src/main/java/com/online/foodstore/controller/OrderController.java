package com.online.foodstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.foodstore.dto.OrderRequestDto;
import com.online.foodstore.dto.ResponseDto;
import com.online.foodstore.exception.ItemsNotFoundException;
import com.online.foodstore.exception.UserNotFoundException;
import com.online.foodstore.exception.VendorNotFoundException;
import com.online.foodstore.service.OrderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author janbee
 *
 */
@RestController
@RequestMapping("/orders")
@Api("Operations pertaining to Orders")
public class OrderController {

	@Autowired
	OrderService orderService;
	
	@PostMapping
	@ApiOperation("Order")
	@ApiResponses({@ApiResponse(code = 7000,message = "User is not found"),
		@ApiResponse(code = 7001,message = "Vendor not found"),
		@ApiResponse(code = 7002,message = "Items not found")})
	public ResponseEntity<ResponseDto> order(@RequestBody OrderRequestDto requestDto) throws UserNotFoundException, VendorNotFoundException, ItemsNotFoundException {
		ResponseDto response = orderService.order(requestDto);
		return new ResponseEntity<>(response,HttpStatus.CREATED);
	}
	
	@GetMapping("/{orderId}")
	public ResponseEntity<Boolean> checkAvailabilityOfOrder(@PathVariable Integer orderId){
		boolean isAvailable = orderService.checkAvailabilityOfOrder(orderId);
		return new ResponseEntity<>(isAvailable,HttpStatus.OK);
		
	}
}
