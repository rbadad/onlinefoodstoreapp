package com.online.foodstore.controller;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.foodstore.entity.Order;
import com.online.foodstore.entity.User;
import com.online.foodstore.exception.OrderHistoryNotFound;
import com.online.foodstore.service.OrderServiceImpl;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Raja
 *
 */
@RestController
@RequestMapping("/users")
public class UserController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private OrderServiceImpl orderServiceImpl;
	
	@GetMapping("/{userId}/orders")
	@ApiOperation("getting user online order history details")
	@ApiResponses(value = { @ApiResponse(code = 1001, message = "USER HAS NO ONLINE ORDER HISTORY") })
	public ResponseEntity<Object> getUserOrderDetails(@PathVariable("userId") Integer userId) {
		LOGGER.info("********Inside getUserOrderDetails method ********** START");
		User user = new User();
		user.setUserId(userId);
		Object order = new ArrayList<Order>();
		try {
			LOGGER.info("********Calling service ********** START");
			order = orderServiceImpl.getUserOrderDetails(user);
		} catch (OrderHistoryNotFound e) {
			LOGGER.error("********Calling service ********** exception Occured");
		}
		LOGGER.info("********Inside getUserOrderDetails method ********** END");
		return new ResponseEntity<>(order, HttpStatus.OK);
	}
}
