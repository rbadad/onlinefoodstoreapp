package com.online.foodstore.exception;

public class ItemsNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ItemsNotFoundException() {
		super();
	}

	public ItemsNotFoundException(String message) {
		super(message);
	}

	
}
