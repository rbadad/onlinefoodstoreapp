package com.online.foodstore.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.online.foodstore.constants.AppConstants;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(value = UserNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleUserNotFoundException() {
		ErrorResponse error = new ErrorResponse();
		error.setStatusCode(AppConstants.USER_NOT_FOUND_STATUS_CODE);
		error.setStatusMsg(AppConstants.USER_NOT_FOUND);
		return new ResponseEntity<>(error,HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = VendorNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleVendorNotFoundException() {
		ErrorResponse error = new ErrorResponse();
		error.setStatusCode(AppConstants.VENDOR_NOT_FOUND_STATUS_CODE);
		error.setStatusMsg(AppConstants.VENDOR_NOT_FOUND);
		return new ResponseEntity<>(error,HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = ItemsNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleItemsNotFoundException() {
		ErrorResponse error = new ErrorResponse();
		error.setStatusCode(AppConstants.ITEMS_NOT_FOUND_STATUS_CODE);
		error.setStatusMsg(AppConstants.ITEMS_NOT_FOUND);
		return new ResponseEntity<>(error,HttpStatus.NOT_FOUND);
	}
}
