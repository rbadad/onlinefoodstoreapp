package com.online.foodstore.constants;

public class AppConstants {

	public static final String USER_NOT_FOUND ="User is not found";
	public static final String USER_NOT_FOUND_STATUS_CODE = "7000";
	
	public static final String ORDERED_SUCCESSFULLY ="Ordered successfully";
	public static final String ORDERED_SUCCESSFULLY_STATUS_CODE = "2000";
	
	public static final String ORDER_CANCELLED ="Order cancelled";
	public static final String ORDER_CANCELLED_STATUS_CODE = "2001";
	
	public static final String VENDOR_NOT_FOUND ="Vendor not found";
	public static final String VENDOR_NOT_FOUND_STATUS_CODE = "7001";
	
	public static final String ITEMS_NOT_FOUND ="Items not found";
	public static final String ITEMS_NOT_FOUND_STATUS_CODE = "7002";
	
	
	public static final String VENDOR_API_BASE_URL = "http://localhost:2021/vendor-api";
	public static final String CHECK_AVAILABILITY_OF_VENDOR_URL= VENDOR_API_BASE_URL+ "/vendors/{vendorId}";
	public static final String CHECK_AVAILABILITY_OF_ITEMS_URL= VENDOR_API_BASE_URL  +"/items";
	
	public static final String NO_ORDER_FOUND_CODE = "1001";
	public static final String NO_ORDER_FOUND_MSG = "USER HAS NO ONLINE ORDER HISTORY";

}
