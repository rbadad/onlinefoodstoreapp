package com.online.foodstore.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MULTIPLE_ITEMS")
public class MultipleItems implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer multipleItemId;
	private Integer itemId;
	@ManyToOne
	@JoinColumn(name = "orderId")
	private Order order;
	public MultipleItems() {
		super();
	}
	public MultipleItems(Integer multipleItemId, Integer itemId, Order order) {
		super();
		this.multipleItemId = multipleItemId;
		this.itemId = itemId;
		this.order = order;
	}
	public Integer getMultipleItemId() {
		return multipleItemId;
	}
	public void setMultipleItemId(Integer multipleItemId) {
		this.multipleItemId = multipleItemId;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	@Override
	public String toString() {
		return "MultipleItems [multipleItemId=" + multipleItemId + ", itemId=" + itemId + ", order=" + order + "]";
	}
	
	
}
