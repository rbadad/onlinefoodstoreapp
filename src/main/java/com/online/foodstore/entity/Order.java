package com.online.foodstore.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ORDERS")
public class Order implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer orderId;
	private Integer vendorId;
	private double totalAmount;
	private LocalDate orderedDate;
	@ManyToOne
	@JoinColumn(name = "userId")
	private User user;
	
	public Order() {
		super();
	}
	
	public Order(Integer orderId, Integer vendorId, double totalAmount, LocalDate orderedDate, User user) {
		super();
		this.orderId = orderId;
		this.vendorId = vendorId;
		this.totalAmount = totalAmount;
		this.orderedDate = orderedDate;
		this.user = user;
	}

	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public Integer getVendorId() {
		return vendorId;
	}
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public LocalDate getOrderedDate() {
		return orderedDate;
	}
	public void setOrderedDate(LocalDate orderedDate) {
		this.orderedDate = orderedDate;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", vendorId=" + vendorId + ", totalAmount=" + totalAmount
				+ ", orderedDate=" + orderedDate + ", user=" + user + "]";
	}
}

