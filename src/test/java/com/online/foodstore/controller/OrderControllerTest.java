package com.online.foodstore.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.online.foodstore.dto.ItemDto;
import com.online.foodstore.dto.OrderRequestDto;
import com.online.foodstore.dto.ResponseDto;
import com.online.foodstore.exception.ItemsNotFoundException;
import com.online.foodstore.exception.UserNotFoundException;
import com.online.foodstore.exception.VendorNotFoundException;
import com.online.foodstore.repository.OrderRepository;
import com.online.foodstore.service.OrderService;

class OrderControllerTest {

	@InjectMocks
	OrderController orderController;
	@Mock
	OrderService orderService;
	@Mock
	OrderRepository orderRepository;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testOrder() throws UserNotFoundException, VendorNotFoundException, ItemsNotFoundException {
		OrderRequestDto orderRequsetDto = new OrderRequestDto();
		orderRequsetDto.setVendorId(1);
		orderRequsetDto.setUserId(1);
		orderRequsetDto.setTotalAmount(6000);
		List<ItemDto> itemList = new ArrayList<>();
		ItemDto itemDto = new ItemDto();
		itemDto.setItemId(1);
		ItemDto itemDto1= new ItemDto();
		itemDto1.setItemId(2);
		itemList.add(itemDto);
		itemList.add(itemDto1);
		orderRequsetDto.setItems(itemList);
		
		ResponseDto response = new ResponseDto();
		response.setStatusCode("1000");
		response.setStatusMsg("ordered successfully");
		
		doReturn(response).when(orderService).order(orderRequsetDto);
		orderController.order(orderRequsetDto);
		assertEquals("1000", response.getStatusCode());
	}
	
	@Test
	public void testCheckAvailabilityOfOrder() {
		Integer orderId = 1;
		boolean isAvaialble = true;
		doReturn(isAvaialble).when(orderService).checkAvailabilityOfOrder(orderId);
		orderController.checkAvailabilityOfOrder(orderId);
		assertTrue(isAvaialble);
	}

}
