package com.online.foodstore.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.online.foodstore.entity.Order;
import com.online.foodstore.entity.User;
import com.online.foodstore.exception.OrderHistoryNotFound;
import com.online.foodstore.service.OrderServiceImpl;

@SpringBootTest
class UserControllerTest {

	@InjectMocks
	private UserController userController;
	
	@Mock
	private OrderServiceImpl orderServiceImpl;
	
	@Test
	public void testGetUserOrderDetails() throws OrderHistoryNotFound {
		ArrayList<Order> orders = new ArrayList<Order>();
		Order order= new Order();
		order.setOrderId(1);
		User user = new User();
		user.setUserId(1);
		orders.add(order);
		doReturn(orders).when(orderServiceImpl).getUserOrderDetails(user);
		userController.getUserOrderDetails(1);
		assertEquals("1", orders.get(0).getUser().getUserId());
	}

}
