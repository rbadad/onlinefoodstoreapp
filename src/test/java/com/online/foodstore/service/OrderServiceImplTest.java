package com.online.foodstore.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;

import com.online.foodstore.dto.ItemDto;
import com.online.foodstore.dto.OrderRequestDto;
import com.online.foodstore.dto.ResponseDto;
import com.online.foodstore.entity.Item;
import com.online.foodstore.entity.MultipleItems;
import com.online.foodstore.entity.Order;
import com.online.foodstore.entity.User;
import com.online.foodstore.exception.ItemsNotFoundException;
import com.online.foodstore.exception.UserNotFoundException;
import com.online.foodstore.exception.VendorNotFoundException;
import com.online.foodstore.repository.MultipleItemRepository;
import com.online.foodstore.repository.OrderRepository;
import com.online.foodstore.repository.UserRepository;

class OrderServiceImplTest {

	@InjectMocks
	OrderServiceImpl orderServiceImpl;
	@Mock
	OrderRepository orderRepository;
	@Mock
	UserRepository userRepository;
	
	@Mock
	private MultipleItemRepository multipleItemRepository;
	
	@Mock
	private RestTemplate restTemplate;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testOrder1() {
		OrderRequestDto orderRequsetDto = new OrderRequestDto();
		orderRequsetDto.setUserId(1);
		orderRequsetDto.setVendorId(1);
		User user = new User();
		Optional<User> userObj = Optional.of(user);
		try {
			doReturn(userObj).when(userRepository).findById(orderRequsetDto.getUserId());
			orderServiceImpl.order(orderRequsetDto);
		} catch(UserNotFoundException |VendorNotFoundException | ItemsNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testOrder2() throws VendorNotFoundException, ItemsNotFoundException {
		OrderRequestDto orderRequsetDto = new OrderRequestDto();
		orderRequsetDto.setUserId(1);
		orderRequsetDto.setVendorId(1);
		User user = new User();
		user.setUserId(1);
		Optional<User> userObj = Optional.of(user);

		when(userRepository.findById(orderRequsetDto.getUserId())).thenReturn(userObj);
		Order order =new Order();
		Order orderObj= new Order();
		orderObj.setOrderId(1);

		when(orderRepository.save(order)).thenReturn(orderObj);

		ResponseDto response = new ResponseDto();
		response.setStatusCode("2000");
		response.setStatusMsg("ordered successfully");
		assertEquals("2000", response.getStatusCode());

	}

	
	@Test
	public void testOrder3() throws VendorNotFoundException, ItemsNotFoundException {
		OrderRequestDto orderRequsetDto = new OrderRequestDto();
		orderRequsetDto.setUserId(1);
		orderRequsetDto.setVendorId(1);
		User user = new User();
		user.setUserId(1);
		Optional<User> userObj = Optional.of(user);

		when(userRepository.findById(orderRequsetDto.getUserId())).thenReturn(userObj);
		Order order =new Order();
		Order orderObj= new Order();

		when(orderRepository.save(order)).thenReturn(orderObj);

		ResponseDto response = new ResponseDto();
		response.setStatusCode("2001");
		response.setStatusMsg("ordered cancelled");
		assertEquals("2001", response.getStatusCode());

	}
	
	@Test
	public void testOrder4() {
		OrderRequestDto orderRequsetDto = new OrderRequestDto();
		orderRequsetDto.setUserId(1);
		orderRequsetDto.setVendorId(1);
		List<ItemDto> itemList = new ArrayList<>();
		ItemDto itemDto = new ItemDto();
		itemDto.setItemId(1);
		ItemDto itemDto1= new ItemDto();
		itemDto1.setItemId(2);
		itemList.add(itemDto);
		itemList.add(itemDto1);
		orderRequsetDto.setItems(itemList);
		User user = new User();
		Optional<User> userObj = Optional.of(user);
		try {
			doReturn(userObj).when(userRepository).findById(orderRequsetDto.getUserId());
			orderServiceImpl.order(orderRequsetDto);
		} catch(UserNotFoundException |VendorNotFoundException | ItemsNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCheckAvailabilityOfOrder() {
		Integer orderId = 1;
		boolean isAvailable= false;
		doReturn(isAvailable).when(orderRepository).existsById(orderId);
		orderServiceImpl.checkAvailabilityOfOrder(orderId);
		assertEquals(false, isAvailable);
	}
	
	@Test
	public void testCheckAvailabilityOfOrder1() {
		Integer orderId = 1;
		boolean isAvailable= true;
		doReturn(isAvailable).when(orderRepository).existsById(orderId);
		orderServiceImpl.checkAvailabilityOfOrder(orderId);
		assertEquals(true, isAvailable);
	}
	
	@Test
	void testgGetUserOrderDetails() {
		User user = new User();
		user.setUserId(1);
		user.setMobileNo("1234567890");
		List<Order> orderList = new ArrayList<Order>();
		Order order = new Order();
		//order.setOrderedDate(LocalDate.now());
		order.setOrderId(1);
		order.setTotalAmount(123.123);
		order.setUser(user);
		order.setVendorId(1);
		orderList.add(order);
		List<MultipleItems> itemss = new ArrayList<>();
		MultipleItems items = new MultipleItems();
		items.setItemId(1);
		items.setMultipleItemId(1);
		items.setOrder(order);
		itemss.add(items);
		String itemUrl = "http://localhost:2021/vendor-api/items/1";
		try {
			doReturn(orderList).when(orderRepository).findByUser(user);
			doReturn(itemss).when(multipleItemRepository).findByOrder(order);
			doReturn(items).when(restTemplate).getForObject(itemUrl, Item.class);
			orderServiceImpl.getUserOrderDetails(user );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void testgGetUserOrderDetailsElse() {
		User user = new User();
		user.setUserId(1);
		user.setMobileNo("1234567890");
		List<Order> orderList = new ArrayList<Order>();
		Order order = new Order();
		List<MultipleItems> itemss = new ArrayList<>();
		MultipleItems items = new MultipleItems();
		items.setItemId(1);
		items.setMultipleItemId(1);
		items.setOrder(order);
		itemss.add(items);
		String itemUrl = "http://localhost:2021/vendor-api/items/1";
		try {
			doReturn(orderList).when(orderRepository).findByUser(user);
			doReturn(itemss).when(multipleItemRepository).findByOrder(order);
			doReturn(items).when(restTemplate).getForObject(itemUrl, Item.class);
			orderServiceImpl.getUserOrderDetails(user );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
